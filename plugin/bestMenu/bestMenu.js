/**
 * Created by db on 14-9-28.
 */
define(["best", "jquery", "template"], function (best, $,template) {
    var b = best;

    function init(el, config) {
        //传入的数据结构
        /*var defaults = {
            data: {
                start: "start menu ",
                items: ["item-1", "item-2"]
            }
        }*/
        var $el;
        if (b.isElement(el)) {
            $el = $(el);
        } else if (b.isJquery(el)) {
            $el = el;
        } else {
            throw new Error(el + "不是合法的dom或者jQuery对象");
        }

        var tpl = '<div class="b-menu-ctn">' +
            '<div class="b-menu-start">' + '{{start}}' + '</div>' +
            '<div class="b-menu-list">' +
            '{{ each items as item i}}' +
            '<div class="b-menu-item">' + '{{item}}' + '</div>' +
            '{{/each}}' +
            '</div>' +
            '</div>';
        /*var tpl = "<div>"+"{{start}}"+"</div>";*/


        var render = template.compile(tpl);

        $el.html(render(config.data));

        //console.log(template)
    }

    return {
        init: init
    }
});