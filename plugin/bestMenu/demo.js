/**
 * Created by db on 14-9-28.
 */

/*必须要配置好jquery,easing,css,artTemplate这4个模块*/
require.config({
    shim:{
        "jquery":{
            exports:"$"
        }
    },
    paths: {
        "jquery": "../../lib/jquery-1.9.1",
        "easing": "../../lib/jquery.easing.1.3",
        "best": "../../lib/best",
        "template":"../../lib/template"
    }
});

require(["bestMenu"],function(bestMenu){
    bestMenu.init($("#b-menu-ctn"),{
        data:{
            start:"hello",
            items:["aaa","bbb"]
        }
    })
});