/**
 * Created by db on 14-9-28.
 */
define(function () {
    function Best() {
    }

    Best.prototype = {
        version: "0.0.1",
        isElement: function (el) {
            return el && el.nodeType && el.nodeType == 1 ? true : false;
        },
        isJquery: function (el) {
            return el && typeof jQuery === "function" && el.length && el instanceof jQuery ? true : false;
        },
        loadCss: function (url) {
            var link = document.createElement("link");
            link.rel = "stylesheet";
            link.type = "text/css";
            link.href = url;
            document.getElementsByTagName("head")[0].appendChild(link);
        }
    }

    return new Best();
});